#include "SignCollection.h"

SignCollection::SignCollection() {}

SignCollection* SignCollection::GetInstance()
{
	if (instance == nullptr)
		instance = new SignCollection();
	return instance;
}

bool SignCollection::ParseFromFile(std::string fPath)
{
	std::ifstream file("Signs.txt");
	if (!file.is_open())
	{
		std::cerr << "The file is not open\n";
		return false;
	}

	while (!file.eof())
	{
		std::string signName;
		int fromDay, fromMonth, toDay, toMonth;

		file >> fromMonth >> fromDay >> toMonth >> toDay >> signName;
		
		Sign sign(Date(fromDay, fromMonth), Date(toDay, toMonth), signName);
		m_signContainer.push_back(sign);
	}

	return true;
}

std::string SignCollection::GetSign(std::string birthDate)
{
	int birthMonth, birthDay;
	std::string birthMonthString, birthDayString;
	std::stringstream sstream, sstream2;


	birthMonthString = birthDate.substr(0, 2);
	sstream << birthMonthString;
	sstream >> birthMonth;

	birthDayString = birthDate.substr(3, 2);
	sstream2 << birthDayString;
	sstream2 >> birthDay;

	int entryDate = (birthMonth * 100) + birthDay;


	for (int index = 0; index < m_signContainer.size(); ++index)
	{
		if (isDateInRange(entryDate, m_signContainer[index]))
			return m_signContainer[index].GetSign();
	}

	return "";
}

bool SignCollection::isDateInRange(int entryDate, Sign sign)
{
	int startDate = (sign.GetFrom().GetMonth() * 100) + sign.GetFrom().GetDay();
	int endDate = (sign.GetTo().GetMonth() * 100) + sign.GetTo().GetDay();

	if (entryDate >= startDate && entryDate <= endDate)
		return true;

	if (sign.GetSign() == "Capricorn")
		return true;

	return false;
}

