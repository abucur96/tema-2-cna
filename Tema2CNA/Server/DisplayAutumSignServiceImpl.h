#pragma once
#include "DisplayAutumSignOperation.grpc.pb.h"
#include "SignCollection.h"
#include <string.h>

class DisplayAutumSignServiceImpl final : public DisplayAutumSignOperationService::Service
{
public:
	DisplayAutumSignServiceImpl() {};
	::grpc::Status DisplayTheSign(::grpc::ServerContext* context, const ::DisplaySignRequest* request, ::DisplaySign* response) override;
};