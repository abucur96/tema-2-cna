#pragma once
#include"DisplaySignServiceImpl.h"
#include<grpc/grpc.h>
#include<grpcpp/server.h>
#include<grpcpp/server_builder.h>
#include<grpcpp/server_context.h>
#include "SignCollection.h"
#include "DisplaySignOperation.grpc.pb.h"
#include "DisplayWinterSignOperation.grpc.pb.h"
#include "DisplayAutumSignOperation.grpc.pb.h"
#include "DisplaySummerSignOperation.grpc.pb.h"
#include "SignCollection.h"
#include <string.h>
#include "DisplaySpringSignServiceImpl.h"
#include "DisplaySummerSignServiceImpl.h"
#include "DisplayAutumSignServiceImpl.h"
#include "DisplayWinterSignServiceImpl.h"
#include <iostream>
#include <DisplaySignOperation.grpc.pb.h>
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

enum class Season
{
	SPRING,
	SUMMER,
	AUTUM,
	WINTER
};

class DisplaySignServiceImpl final : public DisplaySignOperationService::Service
{
public:
	DisplaySignServiceImpl() {};
	::grpc::Status DisplayTheSign(::grpc::ServerContext* context, const ::DisplaySignRequest* request, ::DisplaySign* response) override;
	Season GetSeason(std::string birthDate);
	void Initialise();
};