#include "Sign.h"

Sign::Sign(Date from, Date to, std::string sign): m_from(from), m_to(to), m_sign(sign)
{
}

Date Sign::GetFrom() const
{
	return m_from;
}

Date Sign::GetTo() const
{
	return m_to;
}

std::string Sign::GetSign() const
{
	return m_sign;
}
