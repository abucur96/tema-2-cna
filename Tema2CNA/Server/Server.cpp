#include"DisplaySignServiceImpl.h"

#include<grpc/grpc.h>
#include<grpcpp/server.h>
#include<grpcpp/server_builder.h>
#include<grpcpp/server_context.h>
#include "SignCollection.h"

SignCollection* SignCollection::instance = nullptr;

int main()
{
	std::string server_address("localhost:8888");
	DisplaySignServiceImpl service;

	::grpc_impl::ServerBuilder serverBuilder;
	serverBuilder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
	serverBuilder.RegisterService(&service);
	std::unique_ptr<::grpc_impl::Server> server(serverBuilder.BuildAndStart());
	SignCollection* signCollection = SignCollection::GetInstance();
	std::cout << "Server listening on " << server_address << std::endl;
	service.Initialise();
	server->Wait();
}