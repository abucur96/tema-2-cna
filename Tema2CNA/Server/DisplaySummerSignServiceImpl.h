#pragma once
#include "DisplaySummerSignOperation.grpc.pb.h"
#include "SignCollection.h"
#include <string.h>

class DisplaySummerSignServiceImpl final : public DisplaySummerSignOperationService::Service
{
public:
	DisplaySummerSignServiceImpl() {};
	::grpc::Status DisplayTheSign(::grpc::ServerContext* context, const ::DisplaySignRequest* request, ::DisplaySign* response) override;
	void GetSeason(std::string birthDate);
};