#include "Date.h"

Date::Date(int day, int month) :m_day(day), m_month(month)
{
}

int Date::GetDay() const
{
	return m_day;
}

int Date::GetMonth() const
{
	return m_month;
}
