#include "DisplaySignServiceImpl.h"

::grpc::Status DisplaySignServiceImpl::DisplayTheSign(::grpc::ServerContext* context, const::DisplaySignRequest* request, ::DisplaySign* response)
{
	std::string birthDate = request->birthdate();

	ClientContext context2;

	switch (GetSeason(birthDate))
	{
	case Season::SPRING:
	{
		auto sum_stub = DisplaySummerSignOperationService::NewStub(grpc::CreateChannel("localhost:7777",
			grpc::InsecureChannelCredentials()));
		sum_stub->DisplayTheSign(&context2, *request, response);
	}
		break;
	case Season::SUMMER:
	{
		auto sum_stub = DisplaySummerSignOperationService::NewStub(grpc::CreateChannel("localhost:7777",
			grpc::InsecureChannelCredentials()));
		sum_stub->DisplayTheSign(&context2, *request, response);
	}
		break;
	case Season::AUTUM:
	{
		auto sum_stub = DisplayAutumSignOperationService::NewStub(grpc::CreateChannel("localhost:7777",
			grpc::InsecureChannelCredentials()));
		sum_stub->DisplayTheSign(&context2, *request, response);
	}
		break;
	case Season::WINTER:
	{
		auto sum_stub = DisplayWinterSignOperationService::NewStub(grpc::CreateChannel("localhost:7777",
			grpc::InsecureChannelCredentials()));
		sum_stub->DisplayTheSign(&context2, *request, response);
	}
		break;
	default:
		break;
	}
	
	return ::grpc::Status::OK;

}
void DisplaySignServiceImpl::Initialise()
{
	grpc_init();
	std::string server_address("localhost:7777");
	DisplaySummerSignServiceImpl service1;
	DisplayAutumSignServiceImpl service2;
	DisplayWinterSignServiceImpl service3;
	DisplaySpringSignServiceImpl service4;
	::grpc_impl::ServerBuilder serverBuilder;
	serverBuilder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
	serverBuilder.RegisterService(&service1);
	serverBuilder.RegisterService(&service2);
	serverBuilder.RegisterService(&service3);
	std::unique_ptr<::grpc_impl::Server> server(serverBuilder.BuildAndStart());
	std::cout << "Server listening on " << server_address << std::endl;
	server->Wait();
}

Season DisplaySignServiceImpl::GetSeason(std::string birthDate)
{
	int birthMonth, birthDay;
	std::string birthMonthString, birthDayString;
	std::stringstream sstream, sstream2;


	birthMonthString = birthDate.substr(0, 2);
	sstream << birthMonthString;
	sstream >> birthMonth;

	Season season;

	switch (birthMonth)
	{
	case 12:
	case 1:
	case 2:
		season = Season::WINTER;
		break;
	case 3:
	case 4:
	case 5:
		season = Season::SUMMER;
		break;
	case 6:
	case 7:
	case 8:
		season = Season::SUMMER;
		break;
	case 9:
	case 10:
	case 11:
		season = Season::AUTUM;
		break;
	default:
		break;
	}

	return season;
}

