#pragma once
#include "Date.h"
#include <string>

class Sign
{
public:
	Sign(Date from, Date to, std::string sign);
	Date GetFrom() const;
	Date GetTo() const;
	std::string GetSign() const;

private:
	Date m_from, m_to;
	std::string m_sign;
};

