#pragma once
#include "DisplaySignOperation.grpc.pb.h"
#include "SignCollection.h"
#include <string.h>

class DisplaySpringSignServiceImpl final : public DisplaySignOperationService::Service
{
public:
	DisplaySpringSignServiceImpl() {};
	::grpc::Status DisplayTheSign(::grpc::ServerContext* context, const ::DisplaySignRequest* request, ::DisplaySign* response) override;
};