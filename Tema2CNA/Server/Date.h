#pragma once

class Date
{

public:
	Date(int month, int day);
	int GetDay() const;
	int GetMonth() const;
private:
	int m_day, m_month;
};

