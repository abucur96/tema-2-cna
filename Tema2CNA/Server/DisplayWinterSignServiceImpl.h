#pragma once
#include "DisplayWinterSignOperation.grpc.pb.h"
#include "SignCollection.h"
#include <string.h>

class DisplayWinterSignServiceImpl final : public DisplayWinterSignOperationService::Service
{
public:
	DisplayWinterSignServiceImpl() {};
	::grpc::Status DisplayTheSign(::grpc::ServerContext* context, const ::DisplaySignRequest* request, ::DisplaySign* response) override;
};