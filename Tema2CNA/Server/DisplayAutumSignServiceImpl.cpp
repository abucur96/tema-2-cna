#include "DisplayAutumSignServiceImpl.h"

::grpc::Status DisplayAutumSignServiceImpl::DisplayTheSign(::grpc::ServerContext* context, const::DisplaySignRequest* request, ::DisplaySign* response)
{
	std::string birthDate = request->birthdate();

	SignCollection* signCollection = SignCollection::GetInstance();
	signCollection->ParseFromFile("Signs.txt");

	std::string resultSign = signCollection->GetSign(birthDate);

	response->set_sign(resultSign);
	return ::grpc::Status::OK;
}