#pragma once
#include "Sign.h"
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>

class SignCollection
{
public:
	static SignCollection* GetInstance();
	bool ParseFromFile(std::string fPath);
	std::string GetSign(std::string birthDate);

private:
	SignCollection();
	bool isDateInRange(int entryDate, Sign sign);


	static SignCollection* instance;
	std::vector<Sign> m_signContainer;
};

