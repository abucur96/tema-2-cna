#include <iostream>
#include <DisplaySignOperation.grpc.pb.h>
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

int main()
{
	grpc_init();
	ClientContext context;
	auto sum_stub = DisplaySignOperationService::NewStub(grpc::CreateChannel("localhost:8888",
		grpc::InsecureChannelCredentials()));
	DisplaySignRequest nameRequest;
	std::cout << "Insert your birthDate here:";

	std::string birthDate;

	std::cin >> birthDate;

	nameRequest.set_birthdate(birthDate);
	DisplaySign response;
	auto status = sum_stub->DisplayTheSign(&context, nameRequest, &response);

	if (status.ok())
	{
		std::cout << "Your sign is: " << response.sign() << '\n';
	}
}